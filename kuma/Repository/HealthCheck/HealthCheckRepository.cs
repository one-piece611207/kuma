﻿using System;
using kuma.Data;
using kuma.Models.HealthCheck;

namespace kuma.Repository.HealthCheck
{
	public class HealthCheckRepository: IHealthCheckRepository
	{

        private readonly KumaContext _context;

		public HealthCheckRepository(KumaContext context)
		{
            _context = context;
		}

        public string getMessageHealthCheck()
        {
            var healthCheckEntity = _context.Healthcheck.SingleOrDefault(e => e.id == 1);
            return healthCheckEntity.message;
        }
    }
}

