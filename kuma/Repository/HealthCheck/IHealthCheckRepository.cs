﻿namespace kuma.Repository.HealthCheck
{
	public interface IHealthCheckRepository
	{
        string getMessageHealthCheck();
    }
}

