﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kuma.Models;
using kuma.Repository.HealthCheck;
using Microsoft.AspNetCore.Mvc;

namespace kuma.Controllers
{
    public class HealthCheckController : Controller
    {

        private readonly IHealthCheckRepository _healthCheckRepository;

        public HealthCheckController(IHealthCheckRepository healthCheckRepository)
        {
            _healthCheckRepository = healthCheckRepository;
        }

        [HttpGet("healthcheck")]
        public HealthCheckResponse HealthCheck()
        {
            var healthcheckResponse = new HealthCheckResponse();
            healthcheckResponse.status = _healthCheckRepository.getMessageHealthCheck();
            healthcheckResponse.version = Environment.GetEnvironmentVariable("API_VERSION");
            
            return healthcheckResponse;
        }
    }
}