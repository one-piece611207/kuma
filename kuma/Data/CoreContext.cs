﻿using kuma.Models.HealthCheck;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace kuma.Data
{
	public class CoreContext: DbContext
	{
        public IConfiguration Configuration { get; }

        public CoreContext(DbContextOptions<KumaContext> options, IConfiguration configuration) : base(options)
        {
            Configuration = configuration;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string schema = "todo";
            modelBuilder.HasDefaultSchema(schema);
            modelBuilder.Entity<HealthCheckEntity>().ToTable("healthcheck");
        }

        public DbSet<HealthCheckEntity> Healthcheck { get; set; }
    }
}

