﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace kuma.Data

{
    public class KumaContext : CoreContext
    {
        public KumaContext(DbContextOptions<KumaContext> options, IConfiguration configuration) : base(options, configuration)
        { }
    }
}