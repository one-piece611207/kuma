#!/bin/bash
echo "Start"

echo $ASPNETCORE_ENVIRONMENT

dbUrl="jdbc:postgresql://postgres:5432/postgres"
dbUserName="postgres"
dbPassword=""

cd /app/Liquibase/workspace && \

java -jar liquibase.jar \
	--driver=org.postgresql.Driver \
	--classpath=./lib/postgresql.jar \
	--url=$dbUrl \
	--changeLogFile=./changelog/changelog-initschema.xml \
	--username=$dbUserName\
	--password=$dbPassword\
	--defaultSchemaName=public\
	--databaseChangeLogLockTableName="kuma_databasechangeloglock"\
	--databaseChangeLogTableName="kuma_databasechangelog"\
	update && \

java -jar liquibase.jar \
	--driver=org.postgresql.Driver \
	--classpath=./lib/postgresql.jar \
	--url=$dbUrl \
	--changeLogFile=./changelog/changelog-master.xml \
	--username=$dbUserName\
	--password=$dbPassword\
	--defaultSchemaName=todo\
	update && cd /app/out \
	&& dotnet kuma.dll
