﻿namespace kuma.Models
{
    public class HealthCheckResponse
    {
        public string status { get; set; }

        public string version { get; set; }
    }
}
