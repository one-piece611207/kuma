﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace kuma.Models.HealthCheck
{
	[Table("healthcheck", Schema = "todo")]
	public class HealthCheckEntity
	{
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [ScaffoldColumn(true)]
        [Column("id")]
        public int id { get; set; }

        [Column("message")]
        public string message { get; set; }
	}
}

